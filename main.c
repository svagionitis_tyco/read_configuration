#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "parse_conf.h"

int read_json( const int fd, char **json );
int write_textfile (const char *text, const int fd);


void usage( char *argv0 ) {
    fprintf( stdout, "Usage: %s <conf json file> <file to write>\n", argv0 );
}

int main (int argc, char *argv[])
{
    int ret = 0;
    int json_fd = 0, fd_write = 0;
    char *conf_json = NULL;
    gwsdk_configuration c;


    /* Check that there is a json file */
    if (argc != 3) {
        usage (argv[0]);
        return -1;
    }

    // Open json file for read only
    json_fd = open (argv[1], O_RDONLY);
    if (json_fd < 0) {
        fprintf (stderr, "%s:%d Error opening file %s for reading\n", __func__,
                 __LINE__, argv[1] );
        ret = -1;
        goto out;
    }

    ret = read_json (json_fd, &conf_json);
    if (ret) {
        fprintf (stderr, "%s:%d Error reading file %s\n", __func__,
                 __LINE__, argv[1] );
        goto out;
    }

    ret = parse_conf (conf_json, &c);
    if (ret) {
        fprintf (stderr, "%s:%d Error parsing json '%s'\n", __func__,
                 __LINE__, conf_json);
        goto out;
    }

    fd_write = open (argv[2], O_CREAT | O_WRONLY | O_TRUNC,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    if (fd_write < 0) {
        fprintf (stderr, "%s:%d Error opening file %s for writing\n", __func__,
                 __LINE__, argv[2]);
        ret = -1;
        goto out;
    }

    ret = write_textfile (conf_json, fd_write);
    if (ret < 0) {
        fprintf (stderr, "%s:%d Error writing data to file %s\n", __func__,
                 __LINE__, argv[2]);
        ret = -1;
        goto out;
    }

out:

    if (conf_json)
        free (conf_json);

    if (json_fd)
        close (json_fd);

    if (fd_write)
        close (fd_write);

    return ret;
}

int read_json( const int fd, char **json )
{
    // Buffer to read the json file
    char buffer_read[ 100 ] = {0};
    size_t buff_length = sizeof( buffer_read );
    ssize_t bytes_read = 0;
    size_t size_reallocate = 0;

    if ( *json )
        *json = NULL;

    while( (bytes_read = read( fd, buffer_read, buff_length )) ) {

        char *tmp = NULL;
        size_reallocate += bytes_read * sizeof( char );

        // Allocate bytes_read bytes for the buffer_json
        tmp = realloc( *json, size_reallocate + 1 );
        if ( tmp != NULL ) {
            *json = tmp;
        } else {
            fprintf( stderr, "%s:%d Mem Alloc failed\n", __func__, __LINE__ );

            if ( *json ) {
                free( *json );
                *json = NULL;
            }

            return -1;
        }

        strncat( *json, buffer_read, bytes_read );
        (*json)[ size_reallocate ] = '\0';

    }

    return 0;
}

int
write_textfile (const char *text, const int fd)
{
    size_t text_length, max_buf_sz = 128;
    ssize_t bytes_write;
    const char *t = text;

    if (!text)
        return -1;
    if (fd == -1)
        return -1;

    text_length = strlen (text);

    while ((size_t)(t-text) <= text_length) {
        size_t size_to_write = (text_length - (size_t)(t-text) > max_buf_sz)
                               ? max_buf_sz
                               : text_length - (size_t)(t-text);
        bytes_write = write (fd, t, size_to_write);
        if (bytes_write < 0) {
            fprintf (stderr, "%s: Error writing to file: %d\n",
                     __func__, errno);
            return -1;
        }
        if (!bytes_write)
            break;

        t += bytes_write;
    }

    return 0;
}

