PROJECT=gwsdk_conf

ifndef srcdir
    srcdir = .
endif

ifndef incdir
    incdir = .
endif


CC ?= gcc
CFLAGS=-O0 -Wall -W -ansi -pedantic -std=gnu99 -g -ggdb -I ${incdir}

LIB_DIRS = ${blddir}/lib /opt/local/lib
LINK_DIRS = $(LIB_DIRS:%=-L%)
RUN_DIRS = $(LIB_DIRS:%=-Wl,-rpath,%)
LDFLAGS= ${LINK_DIRS} ${RUN_DIRS}

JSONC_LIB=-ljson-c

LDLIBS=${JSONC_LIB}

SRC_FILES = $(wildcard $(srcdir)/*.c)
OBJ_FILES = $(SRC_FILES:.c=.o)
HDR_FILES = $(incdir)/*.h

all: ${PROJECT}

${PROJECT}: ${OBJ_FILES}
	${CC} ${CFLAGS} -o $@ ${OBJ_FILES} ${LDFLAGS} ${LDLIBS}

.c.o: $(SRC_FILES) ${HDR_FILES}
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf ${OBJ_FILES} ${PROJECT}
