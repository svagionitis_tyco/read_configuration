#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <json-c/json.h>

#include "parse_conf.h"

static int copy_string_object_to_char_array( char char_array[], size_t char_array_sz, struct json_object *json_obj_included, const char *json_field_name, int is_mandatory ) {
    int ret = 0;
    struct json_object *string_obj = NULL;

    json_bool string_obj_exists = json_object_object_get_ex( json_obj_included, json_field_name, &string_obj );
    /* Get the json_field_name */
    if ( !string_obj_exists && is_mandatory ) {
        /* If the json field is null and it's mandatory, return error */
        fprintf( stderr, "%s:%d %s is null.\n", __func__, __LINE__, json_field_name );
        ret = -1;
        goto out;
    } else if ( !string_obj_exists && !is_mandatory ) {
        /* If the json field is null and it's *NOT* madatory, return success */
        goto out;
    } else {
        int json_field_value_len = json_object_get_string_len( string_obj );

        /* Check if the length of the value is 0 */
        if ( json_field_value_len == 0 && is_mandatory ) {
            fprintf( stderr, "%s:%d The value of %s is empty.\n", __func__, __LINE__, json_field_name );
            ret = -1;
            goto out;
        } else if ( json_field_value_len ) {
            /* Check if the size of json value is less or equal the size of the char array */
            if ( (size_t)(json_field_value_len + 1) <= char_array_sz ) {
                // Maybe it would be better to use memcpy_s??
                memcpy( char_array, json_object_get_string( string_obj ), char_array_sz );
            } else {
                fprintf( stderr, "%s:%d Size of char array, %lu, is less than the size of the string, %d.\n", __func__, __LINE__,
                         char_array_sz, json_field_value_len + 1 );
                ret = -1;
                goto out;
            }
        }

    }

out:

    return ret;
}

static int parse_sso_token( struct json_object *SSOToken_obj, sso_token *token ) {
    int ret = 0;

    // Get the successUrl
    ret = copy_string_object_to_char_array( token->success_url, sizeof( token->success_url ), SSOToken_obj, "successUrl", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting successUrl.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the token_id
    ret = copy_string_object_to_char_array( token->token_id, sizeof( token->token_id ), SSOToken_obj, "tokenId", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting tokenId.\n", __func__, __LINE__ );
        goto out;
    }

out:

    return ret;
}

static int parse_oauth_token( struct json_object *oAuthToken_obj, oauth_token *token ) {
    int ret = 0;

    // Get the access_token
    ret = copy_string_object_to_char_array( token->access_token, sizeof( token->access_token ), oAuthToken_obj, "access_token", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting access_token.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the expires_in
    ret = copy_string_object_to_char_array( token->expires_in, sizeof( token->expires_in ), oAuthToken_obj, "expires_in", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting expires_in.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the refresh_token
    ret = copy_string_object_to_char_array( token->refresh_token, sizeof( token->refresh_token ), oAuthToken_obj, "refresh_token", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting refresh_token.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the scope
    ret = copy_string_object_to_char_array( token->scope, sizeof( token->scope ), oAuthToken_obj, "scope", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting scope.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the token_type
    ret = copy_string_object_to_char_array( token->token_type, sizeof( token->token_type ), oAuthToken_obj, "token_type", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting token_type.\n", __func__, __LINE__ );
        goto out;
    }

out:

    return ret;
}

int parse_conf( const char *json, gwsdk_configuration *conf ) {
    const char *conf_json = json;
    struct json_object *obj;
    enum json_tokener_error jerr;
    int ret = 0;

    if ( json == NULL || strlen( json ) == 0 ) {
        fprintf( stderr, "%s:%d No json response was given.\n", __func__, __LINE__ );
        return -1;
    }

    // Start parsing
    obj = json_tokener_parse_verbose( conf_json, &jerr );
    if ( obj == NULL && jerr != json_tokener_success ) {
        fprintf( stderr, "%s:%d Error parsing: %s\n", __func__, __LINE__, json_tokener_error_desc( jerr ) );
        ret = -1;
        goto out;
    }

    // Get the deviceUUID
    ret = copy_string_object_to_char_array( conf->device_uuid, sizeof( conf->device_uuid ), obj, "deviceUUID", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting deviceUUID.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the currentPSK
    ret = copy_string_object_to_char_array( conf->current_psk, sizeof( conf->current_psk ), obj, "currentPSK", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting currentPSK.\n", __func__, __LINE__ );
        goto out;
    }

    // Get the serialNumber
    ret = copy_string_object_to_char_array( conf->serial_number, sizeof( conf->serial_number ), obj, "serialNumber", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting serialNumber.\n", __func__, __LINE__ );
        goto out;
    }

    /////////////////// Parse oAuthToken ////////////////////////
    struct json_object *oAuthToken_obj;
    json_bool oAuthToken_exists = json_object_object_get_ex( obj, "oAuthToken", &oAuthToken_obj );
    if ( !oAuthToken_exists ) {
        fprintf( stderr, "%s:%d oAuthToken does not exist.\n", __func__, __LINE__ );
        ret = -1;
        goto out;
    } else {
        ret = parse_oauth_token( oAuthToken_obj, &conf->otoken );
        if ( ret ) {
            fprintf( stderr, "%s:%d Error parsing oAuthToken.\n", __func__, __LINE__ );
            goto out;
        }
    } // oAuthToken_exists

    /////////////////// Parse SSOToken ////////////////////////
    struct json_object *SSOToken_obj;
    json_bool SSOToken_exists = json_object_object_get_ex( obj, "SSOToken", &SSOToken_obj );
    if ( !SSOToken_exists ) {
        fprintf( stderr, "%s:%d SSOToken does not exist.\n", __func__, __LINE__ );
        ret = -1;
        goto out;
    } else {
        ret = parse_sso_token( SSOToken_obj, &conf->stoken );
        if ( ret ) {
            fprintf( stderr, "%s:%d Error parsing SSOToken.\n", __func__, __LINE__ );
            goto out;
        }
    } // SSOToken_exists

    ret = copy_string_object_to_char_array( conf->s3_endpoint, sizeof( conf->s3_endpoint ), obj, "s3Endpoint", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting s3Endpoint.\n", __func__, __LINE__ );
        goto out;
    }

    ret = copy_string_object_to_char_array( conf->s3_secretkey, sizeof( conf->s3_secretkey ), obj, "s3SecretKey", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting s3SecretKey.\n", __func__, __LINE__ );
        goto out;
    }

    ret = copy_string_object_to_char_array( conf->s3_accesskey, sizeof( conf->s3_accesskey ), obj, "s3AccessKey", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting s3AccessKey.\n", __func__, __LINE__ );
        goto out;
    }


    ret = copy_string_object_to_char_array( conf->primary_mqtt_broker, sizeof( conf->primary_mqtt_broker ), obj, "primaryMqttBrokerEndpoint", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting primaryMqttBrokerEndpoint.\n", __func__, __LINE__ );
        goto out;
    }

    ret = copy_string_object_to_char_array( conf->secondary_mqtt_broker, sizeof( conf->secondary_mqtt_broker ), obj, "secondaryMqttBrokerEndpoint", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting secondaryMqttBrokerEndpoint.\n", __func__, __LINE__ );
        goto out;
    }

    ret = copy_string_object_to_char_array( conf->registration_service, sizeof( conf->registration_service ), obj, "registrationServiceEndPoint", 1 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting registrationServiceEndPoint.\n", __func__, __LINE__ );
        goto out;
    }

    ret = copy_string_object_to_char_array( conf->certificate_service, sizeof( conf->certificate_service ), obj, "certificateServiceEndPoint", 0 );
    if ( ret ) {
        fprintf( stderr, "%s:%d Error getting certificateServiceEndPoint.\n", __func__, __LINE__ );
        goto out;
    }

out:

    if ( obj ) {
        json_object_put( obj );
        obj = NULL;
    }

    return ret;
}

