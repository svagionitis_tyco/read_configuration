#ifndef PARSE_CONF_H
#define PARSE_CONF_H

#include <stdio.h>

typedef struct sso_token {
    char success_url[ FILENAME_MAX ];
    char token_id[ FILENAME_MAX ];
}sso_token;

typedef struct oauth_token {
    char access_token[ FILENAME_MAX ];
    char expires_in[ FILENAME_MAX ];
    char refresh_token[ FILENAME_MAX ];
    char scope[ FILENAME_MAX ];
    char token_type[ FILENAME_MAX ];
}oauth_token;

typedef struct gwsdk_configuration {
    char device_uuid[ 37 ]; // 36 + 1
    char current_psk[ 65 ]; // 64 + 1
    char serial_number[ FILENAME_MAX ];
    oauth_token otoken;
    sso_token stoken;
    char s3_endpoint[ FILENAME_MAX ];
    char s3_secretkey[ FILENAME_MAX ];
    char s3_accesskey[ FILENAME_MAX ];
    char primary_mqtt_broker[ FILENAME_MAX ];
    char secondary_mqtt_broker[ FILENAME_MAX ];
    char registration_service[ FILENAME_MAX ];
    char certificate_service[ FILENAME_MAX ];
}gwsdk_configuration;

int parse_conf( const char *json, gwsdk_configuration *conf );

#endif
